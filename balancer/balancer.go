package balancer

import (
	"context"
	"errors"
	"fmt"
	"golang.org/x/exp/slog"
	"golang.org/x/sync/errgroup"
	"math"
)

var (
	ErrWorkerContextCancelled = errors.New("context cancelled")
)

type Client interface {
	// Weight is unit-less number that determines how much processing capacity can a client be allocated
	// when running in parallel with other clients. The higher the weight, the more capacity the client receives.
	Weight() int
	// Workload returns a channel of work chunks that are ment to be processed through the Server.
	// Client's channel is always filled with work chunks.
	Workload(ctx context.Context) chan int
}

// Server defines methods required to process client's work chunks (requests).
type Server interface {
	// Process takes one work chunk (request) and does something with it. The error can be ignored.
	Process(ctx context.Context, workChunk int) error
}

// Balancer makes sure the Server is not smashed with incoming requests (work chunks) by only enabling certain number
// of parallel requests processed by the Server. Imagine there's a SLO defined, and we don't want to make the expensive
// service people angry.
//
// If implementing more advanced balancer, ake sure to correctly assign processing capacity to a client based on other
// clients currently in process.
// To give an example of this, imagine there's a maximum number of work chunks set to 100 and there are two clients
// registered, both with the same priority. When they are both served in parallel, each of them gets to send
// 50 chunks at the same time.
// In the same scenario, if there were two clients with priority 1 and one client with priority 2, the first
// two would be allowed to send 25 requests and the other one would send 50. It's likely that the one sending 50 would
// be served faster, finishing the work early, meaning that it would no longer be necessary that those first two
// clients only send 25 each but can and should use the remaining capacity and send 50 again.
type Balancer struct {
	MaxParallelChunk     int32
	queue                BlockingQueue
	server               Server
	maxBatchSize         int
	registrationIDSource int
	closeChan            chan bool
}

type ClientRegistration struct {
	workload chan int
	ctx      context.Context
	id       int
}

// New creates a new Balancer instance. It needs the server that it's going to balance for and a maximum number of work
// chunks that can the processor process at a time. THIS IS A HARD REQUIREMENT - THE SERVICE CANNOT PROCESS MORE THAN
// <PROVIDED NUMBER> OF WORK CHUNKS IN PARALLEL.
func New(s Server, maxParallelChunk int32) *Balancer {
	return &Balancer{
		MaxParallelChunk: maxParallelChunk,
		queue:            NewBlockingQueue(int(maxParallelChunk)),
		server:           s,
		maxBatchSize:     int(maxParallelChunk), //we allow Max process maxParallelChunk in parallel
		closeChan:        make(chan bool),
	}
}

func (b *Balancer) Start(ctx context.Context) {
	go b.scheduler(ctx)
}

// Close clean-up the balancer resources
func (b *Balancer) Close() <-chan bool {
	slog.Info("Closing load balancer")
	b.queue.Close()
	return b.closeChan
}

// Register a client to the balancer and start processing its work chunks through provided processor (server).
// For the sake of simplicity, assume that the client has no identifier, meaning the same client can register themselves
// multiple times.
func (b *Balancer) Register(ctx context.Context, client Client) {

	b.queue.Enqueue(ctx, &ClientRegistration{
		workload: client.Workload(ctx),
		ctx:      ctx,
		id:       b.registrationIDSource,
	})
	b.registrationIDSource++
}

func (b *Balancer) scheduler(ctx context.Context) {
	// TODO use context to cancel the scheduler
	for {
		inQueue := b.queue.Len()
		slog.Info(fmt.Sprintf("[SCHEDULER] Detected %d registrations in queue", inQueue))
		// we schedule minimum of 1 worker even if there is no client registered
		scheduleWorkers := int(math.Max(1, math.Min(float64(b.maxBatchSize), float64(inQueue))))
		maxChunksPerWorker := int(b.MaxParallelChunk) / scheduleWorkers
		slog.Info(fmt.Sprintf("[SCHEDULER] Scheduling %d workers, max %d chunks per worker", scheduleWorkers, maxChunksPerWorker))
		if err := b.ScheduleBatch(ctx, scheduleWorkers, maxChunksPerWorker); errors.Is(err, ErrWorkerContextCancelled) {
			slog.Info("[SCHEDULER] Scheduler closed")
			b.closeChan <- true
			break
		}
	}
}

// ScheduleBatch takes numWorkers client registrations and starts scheduling the
// parallel work for each client.
func (b *Balancer) ScheduleBatch(ctx context.Context, numWorkers, maxChunkPerWorker int) error {
	errG := new(errgroup.Group)
	for i := 0; i < numWorkers; i++ {
		// schedule the worker dedicated to client(registration)
		errG.Go(func() error {
			// this is blocking call, so we always wait for client
			workItem := b.queue.Dequeue(ctx)
			if workItem == nil {
				slog.Info(fmt.Sprintf("[WORKER] Context canceled"))
				return ErrWorkerContextCancelled
			}
			isDone := b.ServeClient(workItem, maxChunkPerWorker)
			if !isDone {
				slog.Info(fmt.Sprintf("[C_REG: %d] Back to queue", workItem.id))
				b.queue.Enqueue(ctx, workItem)
			} else {
				slog.Info(fmt.Sprintf("[C_REG: %d] Work done", workItem.id))
			}
			return nil
		})
	}
	err := errG.Wait()
	slog.Info("[BATCH] Batch processed")
	return err
}

// ServeClient parallelize processing of workload on one client. The parallel work is limited to
// maxChunkPerWorker which is always <= MaxParallelChunk.
func (b *Balancer) ServeClient(workItem *ClientRegistration, maxChunkPerWorker int) (isDone bool) {
	slog.Info(fmt.Sprintf("[C_REG: %d] Starts processing %d chunks", workItem.id, maxChunkPerWorker))
	errG := new(errgroup.Group)
	processed := 0
	for i := 0; i < maxChunkPerWorker; i++ {
		load := <-workItem.workload
		if load == 0 {
			isDone = true
			break
		}
		processed++
		// As the b.server allows parallel processing of payload we can async process
		// the payload and do not wait for the previous result. Because we are limited to maxChunkPerWorker
		// coming from scheduler, this ensures the quota on protected service is not exceeded.
		errG.Go(func() error {
			err := b.server.Process(workItem.ctx, load)
			if err != nil {
				slog.Error(fmt.Sprintf("[C_REG: %d] Cannot process chunk: %s", workItem.id, err.Error()))
				return err
			}
			return nil
		})
	}
	if err := errG.Wait(); err != nil {
		slog.Info(fmt.Sprintf("[C_REG: %d] Parallel processing of %d chunks done with error %s", workItem.id, processed, err.Error()))
	} else {
		slog.Info(fmt.Sprintf("[C_REG: %d] Parallel processing of %d chunks done without errors", workItem.id, processed))
	}
	return
}
