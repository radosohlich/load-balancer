package balancer

import (
	"context"
	"fmt"
	"golang.org/x/exp/slog"
)

// BlockingQueue is the queue that blocks for read in case the queue is empty
type BlockingQueue interface {
	Enqueue(ctx context.Context, elem *ClientRegistration)
	Dequeue(ctx context.Context) *ClientRegistration
	Len() int
	Close()
}

type Queue struct {
	Elements chan *ClientRegistration
	qCtx     context.Context
	cancel   context.CancelFunc
}

// NewBlockingQueue creates BlockingQueue that has on given
// max capacity.
func NewBlockingQueue(max int) BlockingQueue {
	c, cancel := context.WithCancel(context.Background())
	return &Queue{
		Elements: make(chan *ClientRegistration, max),
		qCtx:     c,
		cancel:   cancel,
	}
}

func (q *Queue) Close() {
	q.cancel()
}

func (q *Queue) Enqueue(ctx context.Context, elem *ClientRegistration) {
	go func() {
		slog.Info(fmt.Sprintf("[Queue] Added ID:%d to queue", elem.id))
		select {
		case q.Elements <- elem:
		case <-ctx.Done():
			slog.Info(fmt.Sprintf("[Queue] Context canceled job %d deadline reached", elem.id))
		}
	}()
}

func (q *Queue) Dequeue(ctx context.Context) *ClientRegistration {
	select {
	case out := <-q.Elements:
		return out
	case <-ctx.Done():
		return nil
	}
}

func (q *Queue) Len() int {
	return len(q.Elements)
}
